from src.dapp import BasicMathOperation
import time 
import pytest


def test_addition():
    operation = BasicMathOperation(5, 3)
    time.sleep(5)
    assert operation.addition() == 8

def test_subtraction():
    operation = BasicMathOperation(5, 3)
    time.sleep(5)
    assert operation.subtraction() == 2

def test_multiplication():
    operation = BasicMathOperation(5, 3)
    time.sleep(5)
    assert operation.multiplication() == 15

def test_division():
    operation = BasicMathOperation(6, 3)
    time.sleep(5)
    assert operation.division() == 2

def test_division_by_zero():
    operation = BasicMathOperation(5, 0)
    time.sleep(5)
    assert operation.division() == "Cannot divide by zero"
