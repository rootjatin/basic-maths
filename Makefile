.PHONY: test create-pod delete-pod

NAMESPACE=default

create-pod:
	@echo $(NAMESPACE)

delete-pod:
	kubectl delete namespace $(NAMESPACE)

test: create-pod
	FLASK_POD_NAME=$(shell kubectl get pods -l app=basicmath-pod -o jsonpath='{.items[0].metadata.name}' --namespace=$(NAMESPACE)); \
	kubectl run test-runner --image=basicmaths:0.0.1 --rm --attach --restart=Never --namespace=$(NAMESPACE) --command -- /bin/bash -c "python3 -m pip install pytest && cd .. && cd test && pytest -v test_script.py" --env FLASK_POD_NAME=$$FLASK_POD_NAME
