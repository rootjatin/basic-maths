from flask import Flask, jsonify, request

app = Flask(__name__)

class BasicMathOperations:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def addition(self):
        return self.a + self.b

    def subtraction(self):
        return self.a - self.b

    def multiplication(self):
        return self.a * self.b

    def division(self):
        if self.b != 0:
            return self.a / self.b
        else:
            return "Cannot divide by zero"

@app.route('/add', methods=['POST'])
def add():
    data = request.json
    a = data.get('a')
    b = data.get('b')
    operation = BasicMathOperations(a, b)
    result = operation.addition()
    return jsonify({'result': result})

@app.route('/subtract', methods=['POST'])
def subtract():
    data = request.json
    a = data.get('a')
    b = data.get('b')
    operation = BasicMathOperations(a, b)
    result = operation.subtraction()
    return jsonify({'result': result})

@app.route('/multiply', methods=['POST'])
def multiply():
    data = request.json
    a = data.get('a')
    b = data.get('b')
    operation = BasicMathOperations(a, b)
    result = operation.multiplication()
    return jsonify({'result': result})

@app.route('/divide', methods=['POST'])
def divide():
    data = request.json
    a = data.get('a')
    b = data.get('b')
    operation = BasicMathOperations(a, b)
    result = operation.division()
    return jsonify({'result': result})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
